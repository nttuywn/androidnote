package com.tuyen.textview;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.TypefaceSpan;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    final static float STEP = 200;
    TextView mytv;
    float mRatio = 1.0f;
    int mBaseDist;
    float mBaseRatio;
    float fontsize = 13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textview = findViewById(R.id.textView);
        String firstWord = "Booked";
        String lastWord = "lul";
        int color = getResources().getColor(R.color.colorAccent);
        int lastColor = getResources().getColor(R.color.colorPrimaryDark);
        Spannable spannable = new SpannableString(firstWord + lastWord);
        spannable.setSpan(new ForegroundColorSpan(color), 0, firstWord.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(lastColor), firstWord.length(),
                firstWord.length() + lastWord.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textview.setText(spannable);

        TextView textview2 = findViewById(R.id.textView2);
        Spannable spannable2 = new SpannableString(firstWord + lastWord);
        spannable2.setSpan(new RelativeSizeSpan(1.1f), 0, firstWord.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); // set size
        spannable2.setSpan(new RelativeSizeSpan(0.8f), firstWord.length(), firstWord.length() +
                lastWord.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE); // set size
        textview2.setText(spannable2);

        TextView textview3 = findViewById(R.id.textView3);
        Spannable spannable3 = new SpannableString(firstWord + lastWord);
        spannable3.setSpan(new CustomTypefaceSpan("SFUIText-Bold.otf", Typeface.DEFAULT_BOLD), 0,
                firstWord.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable3.setSpan(new CustomTypefaceSpan("SFUIText-Regular.otf", Typeface.MONOSPACE),
                firstWord.length(), firstWord.length() + lastWord.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textview3.setText(spannable3);

        TextView textview4 = findViewById(R.id.textView4);
        String sampleText = "This is a test strike";
        textview4.setPaintFlags(textview4.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        textview4.setText(sampleText);

        TextView textview5 = findViewById(R.id.textView5);
        String sampleText2 = "This is a test strike";
        SpannableStringBuilder spanBuilder = new SpannableStringBuilder(sampleText2);
        StrikethroughSpan strikethroughSpan = new StrikethroughSpan();
        spanBuilder.setSpan(
                strikethroughSpan, // Span to add
                0, // Start
                4, // End of the span (exclusive)
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Text changes will not reflect in the strike changing
        );
        textview5.setText(spanBuilder);

        TextView txtView6 = (TextView) findViewById(R.id.textView6);
        SpannableString spannableString = new SpannableString("RM123.456");
        spannableString.setSpan(new TopAlignSuperscriptSpan((float) 0.35), 0, 2,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtView6.setText(spannableString);

        mytv = (TextView) findViewById(R.id.mytv);
        mytv.setTextSize(mRatio + 13);

    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getPointerCount() == 2) {
            int action = event.getAction();
            int pureaction = action & MotionEvent.ACTION_MASK;
            if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
                mBaseDist = getDistance(event);
                mBaseRatio = mRatio;
            } else {
                float delta = (getDistance(event) - mBaseDist) / STEP;
                float multi = (float) Math.pow(2, delta);
                mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
                mytv.setTextSize(mRatio + 13);
            }
        }
        return true;
    }

    int getDistance(MotionEvent event) {
        int dx = (int) (event.getX(0) - event.getX(1));
        int dy = (int) (event.getY(0) - event.getY(1));
        return (int) (Math.sqrt(dx * dx + dy * dy));
    }

    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}

class TopAlignSuperscriptSpan extends SuperscriptSpan {
    //divide superscript by this number
    protected int fontScale = 2;
    //shift value, 0 to 1.0
    protected float shiftPercentage = 0;

    //doesn't shift
    TopAlignSuperscriptSpan() {
    }

    //sets the shift percentage
    TopAlignSuperscriptSpan(float shiftPercentage) {
        if (shiftPercentage > 0.0 && shiftPercentage < 1.0)
            this.shiftPercentage = shiftPercentage;
    }

    @Override
    public void updateDrawState(TextPaint tp) {
//original ascent
        float ascent = tp.ascent();
//scale down the font
        tp.setTextSize(tp.getTextSize() / fontScale);
//get the new font ascent
        float newAscent = tp.getFontMetrics().ascent;
//move baseline to top of old font, then move down size of new font
//adjust for errors with shift percentage
        tp.baselineShift += (ascent - ascent * shiftPercentage)
                - (newAscent - newAscent * shiftPercentage);
    }

    @Override
    public void updateMeasureState(TextPaint tp) {
        updateDrawState(tp);
    }
}

class CustomTypefaceSpan extends TypefaceSpan {
    private final Typeface newType;

    public CustomTypefaceSpan(String family, Typeface type) {
        super(family);
        newType = type;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        applyCustomTypeFace(ds, newType);
    }

    @Override
    public void updateMeasureState(TextPaint paint) {
        applyCustomTypeFace(paint, newType);
    }

    private static void applyCustomTypeFace(Paint paint, Typeface tf) {
        int oldStyle;
        Typeface old = paint.getTypeface();
        if (old == null) {
            oldStyle = 0;
        } else {
            oldStyle = old.getStyle();
        }
        int fake = oldStyle & ~tf.getStyle();
        if ((fake & Typeface.BOLD) != 0) {
            paint.setFakeBoldText(true);
        }
        if ((fake & Typeface.ITALIC) != 0) {
            paint.setTextSkewX(-0.25f);
        }
        paint.setTypeface(tf);
    }
}
