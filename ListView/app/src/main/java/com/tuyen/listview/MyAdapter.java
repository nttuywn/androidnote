package com.tuyen.listview;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends ArrayAdapter<People> {

    private LayoutInflater inflater;

    public MyAdapter(Context context, List<People> data) {
        super(context, 0, data);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public long getItemId(int position) {
//It is just an example
        People data = (People) getItem(position);
        return data.getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        if (view == null) {
            view = inflater.inflate(R.layout.row, null);
            // Do some initialization
            //Retrieve the view on the item layout and set the value.
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        //Retrieve your object
        People data = (People) getItem(position);
        viewHolder.txt.setTypeface(Typeface.DEFAULT_BOLD);
        viewHolder.txt.setText(data.getName());
        viewHolder.img.setImageBitmap(BitmapFactory.decodeFile(data.getImgUrl()));
        return view;
    }

    private class ViewHolder {
        private final TextView txt;
        private final ImageView img;

        private ViewHolder(View view) {
            txt = (TextView) view.findViewById(R.id.lbl_name);
            img = (ImageView) view.findViewById(R.id.imageView);
        }
    }

}
